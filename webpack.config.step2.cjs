const webpack = require("webpack")
const path = require("path")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const TerserPlugin = require("terser-webpack-plugin")
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin")

const config = require("./buildConfig.cjs")
const configForJSConsumption = {PRODUCTION: JSON.stringify(config.mode == "production"), DEBUG: JSON.stringify(config.debug)}


module.exports = {
    mode: config.mode,

    entry: {
        index: "./src/index.js"
    },

    output: {
        filename: config.fileNames,
        path: config.deployPath,
    },

    devServer: {
        contentBase: config.localDeployRoot,
    },

    optimization: {
        minimize: !config.debug,
        minimizer: [new TerserPlugin({
            terserOptions: {
                output: {
                    comments: false,
                },
            },
            extractComments: false,
        })],
        namedChunks: true
    },

    devtool: config.debug ? "source-map" : false,

    resolve: {
        extensions: [".js"],
    },

    module: {
        rules: [
        {
            test: /inline\.css$/i,
            use: config.mode === "development" ? ["style-loader", "css-loader"] : "ignore-loader"
        },
        {
            test: /(style|override)\.css$/i,
            use: [{ loader: "style-loader", options: { injectType: "linkTag"} },
            {
                loader: "file-loader",
                options: {
                    name: config.filePrefix + "[name].[ext]",
                }
            },
            "extract-loader",
            { loader: "css-loader", options: { sourceMap: true } }
           ],
        },
            {
            test: /\.js$/,
            exclude: /node_modules/,
            use: ["babel-loader", "eslint-loader"]
        },
        {
            test: /\.handlebars$/,
            use: {
                    loader: "handlebars-loader",
                    options: {helperDirs: [path.resolve("./src/handlebars/helpers")]},
                 }
        },
        {
            test: /\.(png|jpe?g|gif|svg|woff2?)$/i,
            loader: "file-loader",
            options: {
                name: config.filePrefix + "[name].[ext]"
            }
        },
        ]
    },

    plugins: [
        new webpack.DefinePlugin({ENV: configForJSConsumption}),
        new webpack.optimize.LimitChunkCountPlugin({maxChunks: 2}),
        new OptimizeCssAssetsPlugin(),

        new HtmlWebpackPlugin({
            title: "Anki Production Card (front) Preview",
            template: path.resolve(config.localDeployAnki, "production/previewFront.handlebars"),
            inject: false,
            filename: path.resolve(config.localDeployPreview, "productionFront.html"),
            base: config.previewHTMLBaseTag,
            templateParameters: config.cardPirouette
        }),

        new HtmlWebpackPlugin({
            title: "Anki Production Card (front-back) Preview",
            template: path.resolve(config.localDeployAnki, "production/previewBack.handlebars"),
            inject: false,
            filename: path.resolve(config.localDeployPreview, "productionBack.html"),
            base: config.previewHTMLBaseTag,
            templateParameters: config.cardPirouette
        }),

        new HtmlWebpackPlugin({
            title: "Anki Production From Image Card (front) Preview",
            template: path.resolve(config.localDeployAnki, "productionFromImage/previewFront.handlebars"),
            inject: false,
            filename: path.resolve(config.localDeployPreview, "productionImageFront.html"),
            base: config.previewHTMLBaseTag,
            templateParameters: config.cardPirouette
        }),

        new HtmlWebpackPlugin({
            title: "Anki Production From Image Card (front-back) Preview",
            template: path.resolve(config.localDeployAnki, "productionFromImage/previewBack.handlebars"),
            inject: false,
            filename: path.resolve(config.localDeployPreview, "productionImageBack.html"),
            base: config.previewHTMLBaseTag,
            templateParameters: config.cardButterfly
        }),

        new HtmlWebpackPlugin({
            title: "Anki Recognition Card (front) Preview",
            template: path.resolve(config.localDeployAnki, "recognition/previewFront.handlebars"),
            inject: false,
            filename: path.resolve(config.localDeployPreview, "recognitionFront.html"),
            base: config.previewHTMLBaseTag,
            templateParameters: config.cardWithoutLanguageTag
        }),

        new HtmlWebpackPlugin({
            title: "Anki Recognition Card (front-back) Preview",
            template: path.resolve(config.localDeployAnki, "recognition/previewBack.handlebars"),
            inject: false,
            filename: path.resolve(config.localDeployPreview, "recognitionBack.html"),
            base: config.previewHTMLBaseTag,
            templateParameters: config.cardButterfly
        }),
    ],
}