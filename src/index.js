"use strict"
if(window.LELA_VERSION === undefined) {
    import(/* webpackChunkName: "main" */ "./main.js")
    import("./style.css")

    if(!ENV.PRODUCTION)
        import("./inline.css")
}
