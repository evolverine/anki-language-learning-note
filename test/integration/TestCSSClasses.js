import test from "ava"
import { JSDOM, VirtualConsole } from "jsdom"
import buildConfig from "../../buildConfig.cjs"
import { Card, CardData } from "../../src/include/classes.js"
import { doPolyFills } from "../../src/include/utils.js"

import path from "path"
import HtmlWebpackPlugin from "html-webpack-plugin"
import buildStep2 from "../../webpack.config.step2.cjs"

const ID_BOX_PROMPT = "promptBox"
const ID_BOX_ANSWER_PREVIEW = "answerBoxPreview"
const ID_BOX_ANSWER = "answerBox"

let window
let document
let cardDataByHTML = []
const url = "file:///home/mc/Documents/Projects/AnkiJS/code/_dist/preview/productionFront.html"
const virtualConsole = ENV.DEBUG ? new VirtualConsole().sendTo(console) : new VirtualConsole()
const jsdomOptions = { url: url, runScripts: "dangerously", resources: "usable", virtualConsole: virtualConsole }

test.before(t => {
    doPolyFills()

    buildStep2.plugins.filter(plugin => plugin instanceof HtmlWebpackPlugin)
        .forEach(plugin => cardDataByHTML[path.basename(plugin.options.filename, ".html")] = plugin.options.templateParameters)
})

test("checking that CSS classes are added correctly", async t => {
    t.timeout(8000) // milliseconds
    let html
    
    //const noLanguageTagMessage = "styles for the message boxes should be defined even without a language tag"

    for(let filename in cardDataByHTML) {
        await import(`../../_dist/preview/${filename}.html`).then(module => { html = module.default })

        window = (new JSDOM(html, jsdomOptions)).window
        document = window.document.documentElement

        await new Promise((resolve) => { window.addEventListener("load", resolve) })

        let cardForThisHTML = cardDataByHTML[filename]
        let cardData = new CardData(cardForThisHTML.known, cardForThisHTML.learned, cardForThisHTML.Tags)
        const card = new Card(cardData)

        let isFrontSide = filename.indexOf("Back") === -1
        let isProduction = filename.indexOf("production") !== -1
        let promptLanguage = isProduction ? card.knownLanguage : card.learnedLanguage
        let answerLanguage = (!isProduction) ? card.knownLanguage : card.learnedLanguage
        const areLanguagesDefined = promptLanguage && answerLanguage

        if(areLanguagesDefined) {
            elementShouldHaveClass(t, ID_BOX_PROMPT, promptLanguage, filename)
            elementShouldHaveClass(t, ID_BOX_ANSWER_PREVIEW, answerLanguage, filename)
            if(!isFrontSide) {
                elementShouldHaveClass(t, ID_BOX_ANSWER, answerLanguage, filename)
            }
        } else {
            /*const questionTag = isProduction ? ".known" : ".learned"
            console.log(window.getComputedStyle(document.querySelector(questionTag + " .message")))
            t.truthy(window.getComputedStyle(document.querySelector(questionTag + " .message")).getPropertyValue("background-color"), noLanguageTagMessage + ` (on ${filename})` + document.querySelector("head").outerHTML)

            if(!isFrontSide) {
                const answerTag = isProduction ? ".learned" : ".known"
                t.truthy(window.getComputedStyle(document.querySelector(answerTag + " .message")).getPropertyValue("background-color"), noLanguageTagMessage + ` (on ${filename})` + document.querySelector("head").outerHTML)
            }*/

            t.pass()
        }
    }
})

function elementShouldHaveClass(t, elementID, className, filename) {
    t.true(document.querySelector("#" + elementID).classList.contains(className),
        "the '" + className + "' class should be among the existing classes of #" + elementID + " ([" + document.querySelector("#" + ID_BOX_PROMPT).classList + "]) for " + filename)
}