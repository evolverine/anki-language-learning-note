import path from "path"
import glob from "glob"
import "ava"

for (let file of glob.sync("test/integration/Test*.js")) {
    import(`./${path.basename(file)}`)
}