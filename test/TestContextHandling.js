import test from "ava"
import {setUp, tearDown, cardToMiss} from "./_testUtils.js"
import {insertContextInString, CONTEXT_HTML_WRAPPER} from "../src/index.js"

test.before(t => {
    setUp()
})

test.after.always(t => {
    tearDown()
})

function expectedContext(content) {
    return CONTEXT_HTML_WRAPPER.replace("$1", content)
}

test("test in standard card", t => {
    t.deepEqual(insertContextInString(cardToMiss().known), "to miss " + expectedContext("[💏💌]"), "expecting right context")
})

test("test when no context is present", t => {
    t.deepEqual(insertContextInString("no context"), "no context", "expecting no context replacement")
})

test("test when only one bracket is present at the end", t => {
    t.deepEqual(insertContextInString("no context["), "no context[", "expecting no context replacement")
})

test("test when only one bracket is present in the middle", t => {
    t.deepEqual(insertContextInString("no [context"), "no [context", "expecting no context replacement")
})

test("test when only one bracket is present in the beginning", t => {
    t.deepEqual(insertContextInString("[no context"), "[no context", "expecting no context replacement")
})

test("test when two brackets open the context", t => {
    t.deepEqual(insertContextInString("[[with] context"), expectedContext("[[with]") + " context", "expecting context surrounded by brackets")
})

test("test when two brackets close the context", t => {
    t.deepEqual(insertContextInString("[with]] context"), expectedContext("[with]]") + " context", "expecting context surrounded by brackets")
})

test("test with two contexts", t => {
    t.deepEqual(insertContextInString("with [two] [contexts]"), "with " + expectedContext("[two] [contexts]"), "expecting context surrounded by brackets")
})